package ru.tinkoff.fintech.repository;

import java.util.List;
import java.util.UUID;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.tinkoff.fintech.model.Student;

@Mapper
public interface StudentRepository {

    void save(Student course);

    List<Student> findAll(@Param("ids") List<UUID> ids);

    void update(Student course);

    void addToCourse(UUID courseId, @Param("studentIds") List<UUID> studentIds);

    void delete(UUID id);
}
