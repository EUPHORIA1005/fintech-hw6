package ru.tinkoff.fintech.repository;

import java.util.List;
import java.util.UUID;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.tinkoff.fintech.model.Course;

@Mapper
public interface CourseRepository {

    void save(Course course);

    List<Course> findAll(@Param("ids") List<UUID> ids);

    void update(Course course);

    void delete(UUID id);
}
