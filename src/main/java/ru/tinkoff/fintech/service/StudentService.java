package ru.tinkoff.fintech.service;

import java.util.List;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.model.Student;
import ru.tinkoff.fintech.repository.StudentRepository;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository repository;

    public void save(Student student) {
        repository.save(student);
    }

    public void addToCourse(UUID courseId, List<UUID> studentsId) {
        repository.addToCourse(courseId, studentsId);
    }

    public void update(Student student) {
        repository.update(student);
    }

    public List<Student> findAll(List<UUID> ids) {
        return repository.findAll(ids);
    }

    public void delete(UUID id) {
        repository.delete(id);
    }
}
