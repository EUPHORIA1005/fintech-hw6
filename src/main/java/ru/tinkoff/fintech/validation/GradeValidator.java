package ru.tinkoff.fintech.validation;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import lombok.RequiredArgsConstructor;
import ru.tinkoff.fintech.exception.EntityNotFoundException;
import ru.tinkoff.fintech.model.Course;
import ru.tinkoff.fintech.model.Student;
import ru.tinkoff.fintech.repository.CourseRepository;
import ru.tinkoff.fintech.repository.StudentRepository;
import ru.tinkoff.fintech.resources.dto.AddToCourseRequest;

@RequiredArgsConstructor
public class GradeValidator implements ConstraintValidator<ValidGrade, AddToCourseRequest> {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;

    @Override
    public boolean isValid(AddToCourseRequest request, ConstraintValidatorContext ctx) {
        Course course = Optional.ofNullable(courseRepository.findAll(List.of(request.getCourseId())))
            .filter(Predicate.not(List::isEmpty))
            .map(it -> it.get(0))
            .orElseThrow(() -> new EntityNotFoundException(Course.class));

        List<Student> students = Optional.ofNullable(studentRepository.findAll(request.getStudentIds()))
            .filter(Predicate.not(List::isEmpty))
            .filter(it -> request.getStudentIds().size() == it.size())
            .orElseThrow(() -> new EntityNotFoundException(Student.class));

        return students.stream().map(Student::getGrade).allMatch(it -> it >= course.getRequiredGrade());
    }
}
