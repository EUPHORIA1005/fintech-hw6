package ru.tinkoff.fintech.resources;

import java.util.List;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.model.Course;
import ru.tinkoff.fintech.resources.dto.CourseRequest;
import ru.tinkoff.fintech.service.CourseService;

@RestController
@RequiredArgsConstructor
public class CourseController {

    private final CourseService service;

    @PostMapping("/courses")
    public void create(@RequestBody @Validated CourseRequest request) {
        var course = Course.builder()
            .id(UUID.randomUUID())
            .name(request.getName())
            .description(request.getDescription())
            .requiredGrade(request.getRequiredGrade())
            .build();

        service.save(course);
    }

    @GetMapping("/courses")
    public List<Course> findAll(@RequestParam(required = false) List<UUID> id) {
        return service.findAll(id);
    }

    @PutMapping("/courses/{id}")
    public void update(@PathVariable UUID id, @RequestBody @Validated CourseRequest request) {
        var course = Course.builder()
            .id(id)
            .name(request.getName())
            .requiredGrade(request.getRequiredGrade())
            .description(request.getDescription())
            .build();

        service.update(course);
    }

    @DeleteMapping("/courses/{id}")
    public void delete(@PathVariable UUID id) {
        service.delete(id);
    }
}
