package ru.tinkoff.fintech.resources;

import java.util.List;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.model.Student;
import ru.tinkoff.fintech.resources.dto.AddToCourseRequest;
import ru.tinkoff.fintech.resources.dto.AddToCourseRequest.AddToCourseRequestValidationSequence;
import ru.tinkoff.fintech.resources.dto.StudentRequest;
import ru.tinkoff.fintech.service.StudentService;

@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentService service;

    @PostMapping("/students")
    public void create(@RequestBody @Validated StudentRequest request) {
        var student = Student.builder()
            .id(UUID.randomUUID())
            .name(request.getName())
            .grade(request.getGrade())
            .age(request.getAge())
            .build();

        service.save(student);
    }

    @GetMapping("/students")
    public List<Student> findAll(@RequestParam(required = false) List<UUID> id) {
        return service.findAll(id);
    }

    @PutMapping("/students/{id}")
    public void update(@PathVariable UUID id, @RequestBody @Validated StudentRequest request) {
        var student = Student.builder()
            .id(id)
            .name(request.getName())
            .grade(request.getGrade())
            .age(request.getAge())
            .build();

        service.update(student);
    }

    @DeleteMapping("/students/{id}")
    public void delete(@PathVariable UUID id) {
        service.delete(id);
    }

    @PutMapping("/students/join")
    public void addToCourse(
        @RequestBody @Validated(AddToCourseRequestValidationSequence.class) AddToCourseRequest request
    ) {
        service.addToCourse(request.getCourseId(), request.getStudentIds());
    }

}
