package ru.tinkoff.fintech.resources.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Range;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class StudentRequest {

    @NotEmpty
    String name;
    @NotNull
    @Range(min = 18, max = 65)
    Integer age;
    @NotNull
    Integer grade;
}
