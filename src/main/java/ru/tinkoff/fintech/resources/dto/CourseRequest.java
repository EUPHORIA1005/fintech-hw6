package ru.tinkoff.fintech.resources.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CourseRequest {

    @NotEmpty
    String name;
    @NotEmpty
    String description;
    @NotNull
    Integer requiredGrade;
}
